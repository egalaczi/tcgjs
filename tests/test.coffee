describe "A test suite", () ->
  beforeEach () ->
    return
  afterEach () ->
    return
 
  it 'should fail', () ->
    expect(true).to.be.true;

  it 'deadcard vs card', ->
    dead = new DeadCard(0,0,0)
    alive = new Card("Doge",2,2)
    dead.defend(alive)
    alive.defend(dead)
    expect(dead.def_remaining).to.equal(0)
    expect(alive.def_remaining).to.equal(2)


  it 'should simple game', () ->
    expect(Game).to.be.ok;
    game = new Game
    game.startgame()
    #dogemilk
