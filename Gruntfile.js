module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        src: 'src/app.coffee',
        dest: 'build/app.min.js'
      }
    },
    coffee: {
      compileJoined: {
        options: {
          join: true,
          bare: true
        },
        files: {
          'build/app.js': ['src/*.coffee', ] // concat then compile into single file
        }
      },

    }
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-coffee');
  // Default task(s).
  grunt.registerTask('default', ['coffee']);

};