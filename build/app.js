var Card, DeadCard, Deck, Game, Player,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

Player = (function() {
  function Player(fieldsize) {
    var i;
    this.fieldsize = fieldsize;
    this.hp = 20;
    this.field = (function() {
      var _i, _ref, _results;
      _results = [];
      for (i = _i = 0, _ref = this.fieldsize; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
        _results.push(new DeadCard(0, 0, 0, this));
      }
      return _results;
    }).call(this);
  }

  Player.prototype.pick = function(field) {
    return this.field[field] = this.game.deck.pick();
  };

  return Player;

})();

Game = (function() {
  function Game() {
    console.log("A new game has started...");
    this.deck = new Deck;
    this.player1 = new Player(4);
    this.player2 = new Player(4);
    this.player1.game = this;
    this.player2.game = this;
  }

  Game.prototype.drawTable = function() {
    var card;
    console.log("");
    console.log("Player 1 " + (String.fromCharCode(0x2665)) + " " + this.player1.hp);
    [
      (function() {
        var _i, _len, _ref, _results;
        _ref = this.player1.field;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          card = _ref[_i];
          _results.push(card.draw());
        }
        return _results;
      }).call(this)
    ];
    console.log("=========");
    [
      (function() {
        var _i, _len, _ref, _results;
        _ref = this.player2.field;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          card = _ref[_i];
          _results.push(card.draw());
        }
        return _results;
      }).call(this)
    ];
    console.log("Player 2 " + (String.fromCharCode(0x2665)) + " " + this.player2.hp);
    return console.log("");
  };

  Game.prototype.resolve = function(player1, player2) {
    var dc, i, idx, _i, _len, _ref, _results;
    _ref = player1.field;
    _results = [];
    for (idx = _i = 0, _len = _ref.length; _i < _len; idx = ++_i) {
      i = _ref[idx];
      if (player2.field[idx].defend(player1.field[idx])) {
        dc = new DeadCard(0, 0, 0);
        dc.player = player2;
        _results.push(player2.field[idx] = dc);
      } else {
        _results.push(void 0);
      }
    }
    return _results;
  };

  Game.prototype.startgame = function() {
    var _results;
    this.player1.pick(1);
    this.player1.pick(2);
    this.player2.pick(2);
    this.player2.pick(3);
    this.drawTable();
    _results = [];
    while (this.player1.hp > 0 && this.player2.hp > 0) {
      this.resolve(this.player1, this.player2);
      this.drawTable();
      this.resolve(this.player2, this.player1);
      _results.push(this.drawTable());
    }
    return _results;
  };

  return Game;

})();

Card = (function() {
  function Card(name, att, def) {
    this.name = name;
    this.att = att;
    this.def = def;
    this.def_remaining = this.def;
  }

  Card.prototype.defend = function(card) {
    this.def_remaining = this.def_remaining - card.att;
    return this.def_remaining <= 0;
  };

  Card.prototype.draw = function() {
    return console.log("[" + this.name + "  " + (String.fromCharCode(0x0001F525)) + "  " + this.att + "  " + (String.fromCharCode(0x2665)) + " " + this.def_remaining + "/" + this.def + "]");
  };

  return Card;

})();

DeadCard = (function(_super) {
  var is_dead;

  __extends(DeadCard, _super);

  function DeadCard(name, att, def, player) {
    this.name = name;
    this.att = att;
    this.def = def;
    this.player = player;
    DeadCard.__super__.constructor.call(this, this.name, this.att, this.def);
    if (!this.player) {
      this.player = new Player;
    }
  }

  DeadCard.prototype.defend = function(card) {
    this.player.hp = this.player.hp - card.att;
    if (this.player.hp < 0) {
      return this.player.hp = 0;
    }
  };

  DeadCard.prototype.draw = function() {
    return console.log("[   ]");
  };

  is_dead = true;

  return DeadCard;

})(Card);

Deck = (function() {
  var deck;

  function Deck() {}

  deck = [["Pig", 2, 3], ["Doge", 3, 4], ["Cate", 5, 3]];

  Deck.prototype.pick = function() {
    var card, cardconfig;
    cardconfig = deck[Math.floor(Math.random() * deck.length)];
    card = new Card(cardconfig[0], cardconfig[1], cardconfig[2]);
    return card;
  };

  return Deck;

})();
