class Player
    constructor: (@fieldsize) ->
        @hp = 20
        @field = ( new DeadCard(0,0,0,@) for i in [0..@fieldsize] )

    pick: (field) ->
        @field[field] = @game.deck.pick()



class Game
  constructor: () ->
    console.log "A new game has started..."
    
    @deck = new Deck
    @player1 = new Player(4)
    @player2 = new Player(4)
    @player1.game = @
    @player2.game = @
    
  
  drawTable: () ->
    console.log ""
    console.log "Player 1 #{String.fromCharCode(0x2665)} #{@player1.hp}"
    [card.draw() for card in @player1.field]
    console.log "========="
    [card.draw() for card in @player2.field]
    console.log "Player 2 #{String.fromCharCode(0x2665)} #{@player2.hp}"
    console.log ""

  resolve: (player1, player2) ->
    for i, idx in player1.field
        if player2.field[idx].defend player1.field[idx]
            dc = new DeadCard(0,0,0)
            dc.player = player2
            player2.field[idx] = dc

  startgame: () ->
    @player1.pick(1)
    @player1.pick(2)
    @player2.pick(2)
    @player2.pick(3)
    @drawTable()
    while @player1.hp > 0 and @player2.hp > 0
        @resolve(@player1, @player2)
        @drawTable()
        @resolve(@player2, @player1)
        @drawTable()


