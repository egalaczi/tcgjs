class Card
  constructor: (@name, @att, @def) ->
    @def_remaining = @def
  defend: (card) ->
    @def_remaining = @def_remaining - card.att
    return @def_remaining <= 0
  draw: ->
    console.log "[#{@name}  #{String.fromCharCode(0x0001F525)}  #{@att}  #{String.fromCharCode(0x2665)} #{@def_remaining}/#{@def}]"

class DeadCard extends Card
    constructor: (@name, @att, @def, @player)->
        super(@name, @att, @def)
        if not @player
            @player = new Player
    defend: (card) ->
        @player.hp = @player.hp - card.att
        if @player.hp < 0 
            @player.hp = 0
    draw: ->
        console.log "[   ]"  
    is_dead = true 